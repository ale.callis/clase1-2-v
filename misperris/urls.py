
from django.contrib import admin
from django.urls import path
from aplication import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name="index"),
    path('sistema/peliculas/borrar/<int:id>/',views.borrar,name="borrar_pelicula"),
    path('sistema/peliculas/editar/<int:id>/',views.editar,name="editar_pelicula"),
    path('iniciar_sesion/',views.iniciar_sesion,name="iniciar_sesion"),
    path('cerrar_sesion/',views.cerrar_sesion,name="cerrar_sesion"),
    path('crear/estudio/', views.EstudioCreateView.as_view())
    ]

urlpatterns +=  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
