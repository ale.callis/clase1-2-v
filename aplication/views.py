from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from .models import MonosChinos,Categoria,Estudio
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as log_in, logout as log_out
# Create your views here.


from django.contrib.admin.views.decorators import staff_member_required

def index(request):
    #select * from monoschinos;
    if request.POST:
        nombre = request.POST.get('nombre',False)
        duracion = request.POST.get('duracion',0)
        fecha = request.POST.get('fecha',False)
        categoria = request.POST.get('categoria',False)
        categoria = Categoria.objects.get(id=categoria)
        print(nombre,duracion,fecha)
        mc = MonosChinos(nombre=nombre,duracion=duracion,fecha=fecha,categoria=categoria)
        #insert into monoschinos values(nombre,du) ()
        mc.save()
        return HttpResponseRedirect(reverse("index"))
    context = {'titulo':'Inicio','monoschinos':MonosChinos.objects.all()}
    context['categorias'] = Categoria.objects.all()
    return render(request,'index.html',context)

@staff_member_required
@login_required
def borrar(request,id):
    try:
        mc = MonosChinos.objects.get(id=id)
        mc.delete()
    except Exception:
        print("ERROR")
    return HttpResponseRedirect(reverse("index"))
def editar(request,id):
    try:
        mc = MonosChinos.objects.get(id=id)
        if request.POST:
            nombre = request.POST.get('nombre',False)
            duracion = request.POST.get('duracion',0)
            fecha = request.POST.get('fecha',False)
            print(nombre,duracion,fecha)
            mc.nombre = nombre
            mc.duracion = duracion
            mc.fecha = fecha
            mc.save()
            return HttpResponseRedirect(reverse("index"))
        context = {'monoschinos':MonosChinos.objects.all(),'monochino':mc}
        return render(request,'index.html',context)
    except Exception:
        print("ERROR")
    return HttpResponseRedirect(reverse("index"))

def iniciar_sesion(request):
    if request.POST:
        usuario = request.POST.get('usuario',False)
        contrasenia = request.POST.get('contrasenia',False)
        usuario = authenticate(request=request,username=usuario,password=contrasenia)
        if usuario is not None:
            log_in(request,usuario)
            return HttpResponseRedirect(reverse('index'))
    context = {'titulo':"Inicio de sesión"}
    return render(request,"login.html",context)

def cerrar_sesion(request):
    log_out(request)
    return HttpResponseRedirect(reverse('index'))

class EstudioCreateView(CreateView):
    model = Estudio
    template_name =  'crear.html'
    fields = ('nombre', 'anio_de_fundacion', 'monos_chinos')
    success_url = reverse_lazy('index')

    