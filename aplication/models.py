from django.db import models

# Create your models here.
class Categoria(models.Model):
    nombre = models.CharField(max_length=150)
    edad_recomendada = models.CharField(max_length=10)
    def __str__(self):
        return self.nombre

class MonosChinos(models.Model):
    nombre = models.CharField(max_length=150)
    duracion = models.IntegerField()
    fecha = models.DateField()
    categoria = models.ForeignKey(Categoria,on_delete=models.CASCADE)

class Estudio(models.Model):
    nombre = models.CharField(max_length=150)
    anio_de_fundacion= models.IntegerField()
    monos_chinos = models.ManyToManyField(MonosChinos)